/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.seed.Cola;
import Util.seed.Pila;

/**
 * Programa imprime si una cadena es el inverso de su subcadena cuando son
 * separadas por &
 *
 * @author DOCENTE
 */
public class TestInfijo {

    public static void main(String[] args) {

        String cadena = "UFPS&SPFU"; //--> true
        /**
         * LO QUE ESTÁ ANTES DEL & SE COLOCA EN UNA COLA LO QUE ESTÁ DESPUÉS EN
         * UNA PILA Y SE COMPARA LA PILA Y LA COLA
         */

        String[] datos = cadena.split("&");
        Pila<Character> pila = new Pila<>();
        Cola<Character> cola = new Cola<>();
        boolean iguales = true;

        for (char c : datos[0].toCharArray()) {
            cola.enColar(c);
        }

        for (char c : datos[1].toCharArray()) {

            pila.push(c);

        }

        while (!cola.isEmpty() && iguales == true) {

            if (cola.deColar() != pila.pop()) {

                iguales = false;

            }

        }

        System.out.println(iguales);
    }

}
