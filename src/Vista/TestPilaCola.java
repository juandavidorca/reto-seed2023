/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.seed.Cola;
import Util.seed.Pila;

/**
 *
 * @author DOCENTE
 */
public class TestPilaCola {
    
    public static void main(String[] args) {
        
        Pila<Integer> pila = new Pila<>();
        Cola<Integer> cola = new Cola<>();
        
        for(int i = 0;i<11;i++)
        {
        
            pila.push(i);
            cola.enColar(i);
        
        }
        
        while(!pila.isEmpty())
        {
        
            System.out.print(pila.pop() + "\t");
        
        }
        
        System.out.println("");
        
        while(!cola.isEmpty())
        {
        
            System.out.print(cola.deColar() + "\t");
        
        }        
        
    }
    
}
