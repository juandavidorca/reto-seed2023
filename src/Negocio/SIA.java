/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Negocio;

import Modelo.Estudiante;
import Util.seed.ArchivoLeerURL;
import Util.seed.ListaCD;
import com.itextpdf.text.DocumentException;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DOCENTE
 */
public class SIA {

    private ListaCD<Estudiante> fisica;
    private ListaCD<Estudiante> estructuras;
    private ListaCD<Estudiante> poo;

    public SIA(String urlFisica) {
        this.fisica = crear(urlFisica);
    }

    private ListaCD<Estudiante> crear(String url) {
        ListaCD<Estudiante> l = new ListaCD();
        ArchivoLeerURL archivo = new ArchivoLeerURL(url);
        Object linea[] = archivo.leerArchivo();
        for (int i = 1; i < linea.length; i++) {
            String fila = linea[i].toString();
            //16980,NOMBRE 1,0,2,4,4
            String datos[] = fila.split(",");
            long codigo = Long.parseLong(datos[0]);
            String nombre = datos[1];
            float p1 = Float.parseFloat(datos[2]);
            float p2 = Float.parseFloat(datos[3]);
            float p3 = Float.parseFloat(datos[4]);
            float exm = Float.parseFloat(datos[5]);
            Estudiante nuevo = new Estudiante(codigo, nombre, p1, p2, p3, exm);
            l.insertarFinal(nuevo);
        }
        return l;
    }

    public String getListadoFisica() {
        String msg = "";
        for (Estudiante dato : this.fisica) {
            msg += dato.toString() + "\n";
        }
        return msg;
    }

    public ListaCD<String> getListadoFinal() {
        ListaCD<String> resultado = new ListaCD();
        for (Estudiante x : this.fisica) {
            String m = (x.isAprobado()) ? "Aprobado" : "Reprobado";
            resultado.insertarFinal(x.getCodigo() + "-" + x.getNombre() + "-" + x.getPromedio() + "-" + m+"\n");
        }
        return resultado;
    }
    
    public void getInformePDF()
    {
        try {
            new ImpresoraInformeSIA().imprimirListadoPromedio(this.getListadoFinal());
        } catch (FileNotFoundException | DocumentException ex) {
            Logger.getLogger(SIA.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
